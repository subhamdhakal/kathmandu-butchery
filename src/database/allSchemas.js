const databaseOptions = {
  path: 'realmT4.realm',
  schema: [OrderSchema],
  schemaVersion: 1,
};
import Realm from 'realm';
import {ORDER_SCHEMA, OrderSchema} from './../model/OrderSchema';

export const getOrders = () =>
  new Promise((resolve, reject) => {
    console.log('called reload data all schemas');
    Realm.open(databaseOptions)
      .then((realm) => {
        const res = realm.objects(ORDER_SCHEMA);
        resolve(res);
      })
      .catch((error) => {
        reject(error);
      });
  });

export const addOrder = (order) =>
  new Promise((resolve, reject) => {
    console.log('called reload data all schemas');
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          realm.create(ORDER_SCHEMA, order);
          resolve(order);
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
export const updateOrderNotes = (id, notes) =>
  new Promise((resolve, reject) => {
    console.log('called reload data all schemas');
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let updatingOrderList = realm.objectForPrimaryKey(ORDER_SCHEMA, id);
          updatingOrderList.notes = notes;
        });
      })
      .catch((error) => {
        reject(error);
      });
  });

export const updateOrderQuantity = (id, quantity, updatedPrice) =>
  new Promise((resolve, reject) => {
    console.log('updated price' + updatedPrice);
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let updatingOrderList = realm.objectForPrimaryKey(ORDER_SCHEMA, id);
          updatingOrderList.quantity = parseInt(quantity);
          updatingOrderList.totalPrice = parseInt(updatedPrice);
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
export const deletedIndividialOrder = (id) =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let deletingOrder = realm.objectForPrimaryKey(ORDER_SCHEMA, id);
          realm.delete(deletingOrder);
        });
      })
      .catch((error) => {
        reject(error);
      });
  });

export const deletedAllOrderList = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then((realm) => {
        realm.write(() => {
          let allTodoLists = realm.objects(ORDER_SCHEMA);
          realm.delete(allTodoLists);
        });
      })
      .catch((error) => {
        reject(error);
      });
  });
export default new Realm(databaseOptions);
