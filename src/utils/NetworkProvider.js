import React from 'react';
import NetInfo from '@react-native-community/netinfo';

export const NetworkContext = React.createContext({isConnected: true});

export class NetworkProvider extends React.PureComponent {
  state = {
    isConnected: true,
  };

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener((state) => {
      this.handleConnectivityChange(state.isInternetReachable);
    });
  }

  componentWillUnmount() {
    this.unsubscribe;
  }

  handleConnectivityChange = (isConnected) =>
    this.setState({isConnected: true});

  render() {
    return (
      <NetworkContext.Provider value={this.state}>
        {this.props.children}
      </NetworkContext.Provider>
    );
  }
}
