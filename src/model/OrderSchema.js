export const ORDER_SCHEMA = 'orders';
export const OrderSchema = {
  name: ORDER_SCHEMA,
  primaryKey: 'id',
  properties: {
    id: 'int',
    title: 'string',
    imageUrl: 'string',
    quantity: 'int',
    price: 'int',
    notes: 'string',
    totalPrice: 'int',
  },
};
