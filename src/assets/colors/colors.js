const colors = {
  accentOrange: '#F2932C',
  primary: '#1B2678',
  simrik: '#A6050B',
  textDarkGray: '#D3D3D3',
  white: '#FFFFFF',
  acceptGreen: '#27ae60',
  rejectRed: '#c0392b',
  textInputGray: '#EEF1EF',
  gradientBlue: '#2E79BD',
  accentBlue: '#1B2678',
  black: '#000000',
};
export default colors;
