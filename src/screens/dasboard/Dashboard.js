/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-sparse-arrays */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import {SearchBar} from 'react-native-elements';
import realm from '../../database/allSchemas';
import {addOrder} from '../../database/allSchemas';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput} from '../../components';
var validator = require('email-validator');
import AsyncStorage from '@react-native-community/async-storage';
import {AnimatedFlatList, AnimationType} from 'flatlist-intro-animations';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import colors from './../../assets/colors/colors';
import AntDesign from 'react-native-vector-icons/MaterialCommunityIcons';
import {Avatar, Button} from 'react-native-paper';
import {fetchAllData} from './../../actions/fetchdata';
import {NetworkContext} from '../../utils/NetworkProvider';
import NoInternetModal from '../../components/NoInternetModal';
import Toast from 'react-native-simple-toast';
import {NavHeaderWithLogo} from './../../components/NavHeaderWithLogo';
import {SliderBox} from 'react-native-image-slider-box';
import PushNotification from 'react-native-push-notification';
import * as RootNavigation from './../../services/NavigationService';

export function setupPushNotification(handleNotification) {
  PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
      console.log('TOKEN:', token);
    },
    // (required) Called when a remote or local notification is opened or received
    onNotification: function (notification) {
      console.log('NOTIFICATION:', notification);
      handleNotification(notification);
    },
    // Android only
    senderID: '1009202310164',
    // iOS only
    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },
    requestPermissions: true,
    requestPermissions: Platform.OS === 'ios',
  });
  return PushNotification;
}

class Dashboard extends Component {
  static contextType = NetworkContext;

  state = {
    searchTerm: '',
    data: this.props.productList,
  };
  constructor(props) {
    super(props);
    this.arrayholder = this.props.productList;
  }
  async componentDidMount() {
    // configureNotification(this.props);

    this.pushNotification = setupPushNotification(this._handleNotificationOpen);

    // PushNotification.subscribeToTopic('dataupdated');
    PushNotification.subscribeToTopic(
      JSON.stringify(this.props.userData['id']),
    );
    this.fetchData();
  }
  fetchData() {
    this.props.actions.fetchAllData({
      accessToken: this.props.access_token,
      onSuccess: () => {},
      onFailure: (errorMsg) => {
        alert(errorMsg);
      },
    });
  }
  _handleNotificationOpen = (notification) => {
    // const {navigate} = this.props.navigation;
    this.fetchData();

    console.log('handle open' + JSON.stringify(notification));

    if (notification.data.type === 'orderupdated') {
      if (notification.foreground == true) {
        Alert.alert(notification.title, notification.message, [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'View',
            onPress: () => RootNavigation.navigate('Events'),
          },
        ]);
      } else {
        RootNavigation.navigate('Events');
      }
    }

    // navigate(notification.data.route);
  };
  RenderItem = (item) => (
    <View style={styles.flatlistContainer}>
      <View style={styles.flatlistItem}>
        <View style={styles.leftContainer}>
          <Image
            style={styles.cardImage}
            resizeMode={'cover'}
            source={{
              uri: item.image,
            }}
          />
        </View>
        <View style={styles.RightContainer}>
          <View style={{width: '70%'}}>
            <Text style={styles.nametxt}>{item.product_name}</Text>
            <Text style={styles.addresstxt}>{item.description}</Text>
            <Text style={styles.gendertxt}>${item.price}</Text>
          </View>
          <View
            style={{
              width: '30%',
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                elevation: 5,
                alignContent: 'center',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={() => this.createAnOrderAndPushToArray(item)}>
              <AntDesign name="cart" color={colors.primary} size={26} />
              <Text style={styles.labelText}>Add</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.LastContainer}></View>
      </View>
    </View>
  );

  createAnOrderAndPushToArray = (item) => {
    console.log(JSON.stringify(item));

    const newOrder = {
      id: item.product_id,
      itemId: item.item_code,
      title: item.product_name,
      imageUrl: item.image,
      price: parseInt(item.price),
      quantity: 1,
      notes: '',
      totalPrice: parseInt(item.price),
    };
    addOrder(newOrder)
      .then(() => {
        Toast.show('Item added to cart');

        this.setState({
          cartCount: this.state.cartCount + 1,
        });
      })
      .catch((error) => {
        alert('Item already added to cart');
      });
  };

  updateSearch = (search) => {
    this.setState({search});
  };
  searchFilterFunction = (text) => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter((item) => {
      const itemData = `${item.product_name.toUpperCase()} `;

      // const itemData = `${item.name.title.toUpperCase()} ${item.name.first.toUpperCase()} ${item.name.last.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };

  render() {
    return (
      <View style={styles.Container}>
        <NavHeaderWithLogo
          title={'Products'}
          onPress={() => this.props.navigation.goBack()}
        />
        {this.context.isConnected ? null : <NoInternetModal />}
        <SearchBar
          placeholder="Search Product..."
          onChangeText={(text) => this.searchFilterFunction(text)}
          value={this.state.value}
          clearIcon
          inputStyle={{
            backgroundColor: colors.white,
            fontFamily: 'HelveticaNowDisplay-Regular',
            borderColor: colors.white,
            fontSize: 14,
          }}
          inputContainerStyle={{
            backgroundColor: colors.white,
            borderColor: colors.white,
            height: 32,
          }}
          lightTheme
          containerStyle={{
            backgroundColor: colors.white,
            borderColor: colors.white,
            borderWidth: 0,
            borderRadius: 15,
            height: 48,
            marginLeft: 8,
            marginRight: 8,
          }}
        />
        <SliderBox
          images={this.props.eventImages}
          sliderBoxHeight={h('20%')}
          onCurrentImagePressed={(index) =>
            console.warn(`image ${index} pressed`)
          }
          dotColor="#FFEE58"
          inactiveDotColor="#90A4AE"
          paginationBoxVerticalPadding={20}
          autoplay
          circleLoop
          resizeMethod={'resize'}
          resizeMode={'cover'}
          paginationBoxStyle={{
            position: 'absolute',
            bottom: 0,
            padding: 0,
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            paddingVertical: 10,
          }}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            marginHorizontal: 0,
            padding: 0,
            margin: 0,
            backgroundColor: 'rgba(128, 128, 128, 0.92)',
          }}
          ImageComponentStyle={{borderRadius: 15, width: '97%', marginTop: 2}}
          imageLoadingColor="#2196F3"
        />

        <View style={styles.flatlistContainerView}>
          <AnimatedFlatList
            contentContainerStyle={{
              marginTop: -h('1%'),
            }}
            data={this.state.data}
            renderItem={({item}) => this.RenderItem(item)}
            animationType={AnimationType.SlideFromBottom}
            keyExtractor={(item) => item.product_name}
            animationDuration={1000}
            numColumns={2}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    productList: state.dataReducer.productList,
    eventImages: state.loginReducer.loginResponse['event_images'],
    userData: state.loginReducer.loginResponse['user'],
    access_token: state.loginReducer.loginResponse['token'],
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({fetchAllData}, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  cardImage: {
    flex: 1,
    width: '100%',
    height: '100%',
    borderRadius: 8,
  },
  flatlistItem: {
    backgroundColor: '#fff',
    height: h('24%'),
    flexDirection: 'column',
    borderRadius: h('1%'),
    width: w('46%'),
    margin: 4,
    elevation: 5,
  },
  flatlistContainer: {
    alignItems: 'center',
    marginTop: 8,
  },
  labelText: {
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 10,
  },
  flatlistContainerView: {
    backgroundColor: colors.white,
    height: '90%',
    flex: 1,
    alignContent: 'center',
    alignItems: 'center',
    margin: 8,
    borderRadius: 15,
  },
  leftContainer: {
    // backgroundColor: '#000',
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  RightContainer: {
    // backgroundColor: 'red',
    height: '50%',
    padding: h('1%'),
    justifyContent: 'space-between',
    alignContent: 'center',
    flexDirection: 'row',
  },
  imgcontainer: {
    backgroundColor: '#ea5455',
    borderRadius: h('100%'),
    width: '80%',
    height: '70%',
  },
  LastContainer: {
    // backgroundColor: 'yellow',
    height: '20%',
    justifyContent: 'flex-end',
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  nametxt: {
    color: colors.black,
    fontSize: h('1.2%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  gendertxt: {
    color: colors.primary,
    fontSize: h('1.8%'),
    marginTop: h('1%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  addresstxt: {
    color: 'gray',
    fontSize: h('1.4%'),
    // marginTop: h('1%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  notxt: {
    color: colors.primary,
    fontSize: h('1.2%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  noConatiner: {
    backgroundColor: colors.white,
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: h('1%'),
    borderColor: '#ea5455',
    elevation: 5,
  },
  requestContainr: {
    backgroundColor: '#FF215D',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    paddingLeft: h('2%'),
    // borderRadius: h('10%'),
    marginTop: h('3%'),
    borderTopLeftRadius: h('10%'),
    borderBottomLeftRadius: h('10%'),
  },
  circlebLood: {
    width: '55%',
    height: '40%',
    borderRadius: h('10%'),
    // backgroundColor: 'red',
    marginRight: h('3%'),
    borderColor: 'silver',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row-reverse',
  },
  circelTxt: {
    color: '#FF215D',
    fontSize: h('3%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  requestTxt: {
    color: '#Ffff',
    fontSize: h('2%'),
  },
  frespace: {
    // backgroundColor: 'white',
    width: '20%',
    // marginTop: -h('50%'),
  },
});
