/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/self-closing-comp */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import AnimatedLoader from 'react-native-animated-loader';
import AsyncStorage from '@react-native-community/async-storage';
import colors from '../../assets/colors/colors';
import {color} from 'react-native-reanimated';

export class SplitScreen extends Component {
  componentDidMount = () => {
    setTimeout(() => {
      this.retrieveUser();
    }, 3000);
  };

  retrieveUser = () => {
    AsyncStorage.getItem('save', (err, data) => {
      const User = JSON.parse(data);
      if (false) {
        this.props.navigation.replace('OnboardingScreen');
      } else {
        AsyncStorage.getItem('userdata', (error, data) => {
          const userData = JSON.parse(data);

          if (userData !== null) {
            this.props.navigation.replace('BottomTab');
          } else {
            this.props.navigation.replace('WelcomeScreen');
          }
        });
      }
    });
  };

  render() {
    return (
      <View style={styles.Container}>
        {/* top */}
        <View style={styles.imgContiner}>
          <Image
            style={styles.hand}
            source={require('../../assets/logo.jpg')}
          />
          <View style={styles.Ctext}>
            <ActivityIndicator
              style={{marginRight: h('3%')}}
              size="large"
              color={colors.accentBlue}
            />
            <Text style={styles.txtc}>मासुमा शुद्धता , हाम्रो प्रतिबद्धता</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  imgContiner: {
    // backgroundColor: 'red',
    width: w('100%'),
    height: h('60%'),
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  bg: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    marginTop: h('1.5%'),
  },
  lottie: {
    width: 200,
    height: 200,
  },
  login: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginContainer: {
    // backgroundColor: 'red',
    marginTop: h('13%'),

    width: w('100%'),
    height: h('30%'),
  },
  hand: {
    resizeMode: 'contain',
    height: h('60%'),
    width: w('60%'),
  },
  txt: {
    color: colors.accentBlue,
    fontSize: h('2.5%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },

  btnContainer: {
    backgroundColor: 'tomato',
    height: h('7%'),
    width: '50%',
    borderRadius: h('7%'),
    overflow: 'hidden',
    marginTop: h('2%'),
  },
  btn: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btntxt: {color: 'white', fontWeight: 'bold', fontSize: h('2.8%')},
  emptyContainer: {
    // backgroundColor: 'tomato',
    height: '20%',
    width: w('100%'),
  },
  Ctext: {
    // backgroundColor: 'red',
    width: '100%',
    height: '42%',
    alignItems: 'center',
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  txtc: {
    color: colors.accentBlue,
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 18,
    height: '42%',
  },
});
