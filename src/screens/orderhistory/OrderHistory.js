/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-sparse-arrays */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput} from '../../components';
var validator = require('email-validator');
import AsyncStorage from '@react-native-community/async-storage';
import {AnimatedFlatList, AnimationType} from 'flatlist-intro-animations';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import colors from '../../assets/colors/colors';
import {Portal, Provider} from 'react-native-paper';
import ViewOrderDetailsModal from './../../components/ViewOrderDetailsModal';
import {
  fetcheduserrequestlist,
  placeorder,
  fetchAllData,
  deleteorder,
} from '../../actions/fetchdata';
class OrderHistory extends Component {
  state = {
    detailModal: false,
    orderDetails: [],
  };
  constructor(props) {
    super(props);
    this.dismissCancelModal = this.dismissCancelAppointmentModal.bind(this);
  }
  dismissCancelAppointmentModal() {
    this.setState({
      detailModal: false,
    });
  }

  getStatusColor({status}) {
    switch (status) {
      case 'PENDING':
        return colors.accentOrange;
        break;
      case 'COMPLETE':
        return colors.acceptGreen;
        break;

      case 'PROCESSING':
        return colors.primary;
        break;
      case 'DECLINE':
        return colors.rejectRed;
        break;
      default:
        return colors.rejectRed;

        break;
    }
  }

  RenderItem = (item) => (
    <View style={styles.flatlistContainer}>
      <View style={styles.flatlistItem}>
        <View style={styles.leftContainer}>
          <Text style={styles.datetxt}>{'Date: ' + item.order_date}</Text>
          <Text style={styles.nametxt}>
            {'Total Amount: $' + item.total_amount}
          </Text>
          <Text style={styles.nametxt}>
            {'Total Quantity: ' + item.total_quantity}
          </Text>
        </View>
        <View style={styles.RightContainer}></View>
        <View style={styles.LastContainer}>
          <View
            style={{
              backgroundColor: this.getStatusColor({status: item.status}),
              width: '75%',
              height: '25%',
              justifyContent: 'center',
              paddingLeft: h('1%'),
              // borderRadius: h('10%'),
              marginTop: h('3%'),

              borderTopLeftRadius: h('10%'),
              borderBottomLeftRadius: h('10%'),
            }}>
            <Text style={styles.requestTxt}>{item.status}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignContent: 'flex-end',
              margin: 12,
              justifyContent: 'space-between',
              alignItems: 'stretch',
              alignSelf: 'stretch',
            }}>
            <TouchableOpacity
              style={{elevation: 5, alignItems: 'center'}}
              onPress={() =>
                this.setState({
                  detailModal: true,
                  orderDetails: item,
                })
              }>
              <AntDesign name="eyeo" color={colors.accentBlue} size={24} />
              <Text style={styles.labelText}>View</Text>
            </TouchableOpacity>
            {item.status === 'PENDING' ? (
              <TouchableOpacity
                style={{elevation: 5, alignItems: 'center'}}
                onPress={() => this.deleteOrder(item)}>
                <AntDesign
                  name="closecircleo"
                  color={colors.rejectRed}
                  size={24}
                />
                <Text style={styles.labelText}>Cancel</Text>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    </View>
  );
  deleteOrder = (item) => {
    console.log(JSON.stringify(item));
    // var deleteRequestBody = {
    //   request_id: item.request_id,
    //   user_id: item.user_id,
    // };

    Alert.alert(
      'Delete Order',
      'Are you sure you want to cancal this order ?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Yes',
          onPress: () =>
            this.props.actions.deleteorder({
              orderId: item.id,
              accessToken: this.props.access_token,
              onSuccess: () => {
                // this.toggleLogin(false);
                // this.props.navigation.replace('BottomTab');
                this.props.actions.fetchAllData({
                  accessToken: this.props.access_token,
                  onSuccess: () => {},
                  onFailure: (errorMsg) => {
                    alert(errorMsg);
                  },
                });
              },
              onFailure: () => {
                //Alert error message
                // this.setState({
                //   modalVisible: false,
                // });
              },
            }),
        },
      ],
    );
  };

  render() {
    return (
      <View style={styles.Container}>
        <Provider>
          <Portal>
            <ViewOrderDetailsModal
              visible={this.state.detailModal}
              orderDetails={this.state.orderDetails}
              dismissCancelModal={() => this.dismissCancelModal()}
            />
          </Portal>

          <ImageBackground style={styles.ImageBackground}>
            <Text style={styles.imgtxt}>Order History</Text>
            <View style={styles.newRequest}>
              <View style={styles.left}>
                <Text style={styles.no}>{this.props.orderHistory.length}</Text>
                <Text style={styles.Requst}>Orders</Text>
              </View>
              <View style={styles.right}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('MyCart');
                  }}
                  style={styles.btn}>
                  <AntDesign
                    name="pluscircleo"
                    color={colors.accentBlue}
                    size={20}
                  />
                  <Text style={styles.btntxt}>Add Order</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
          <View style={styles.flatlistContainerView}>
            <AnimatedFlatList
              contentContainerStyle={{
                marginTop: -h('1%'),
              }}
              data={this.props.orderHistory}
              renderItem={({item}) => this.RenderItem(item)}
              animationType={AnimationType.SlideFromRight}
              keyExtractor={(item) => JSON.stringify(item.id)}
              animationDuration={1000}
              focused={true}
            />
          </View>
        </Provider>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    orderHistory: state.dataReducer.orderHistory,
    sliderImages: state.loginReducer.loginResponse['event_images'],
    access_token: state.loginReducer.loginResponse['token'],
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        deleteorder,
        fetchAllData,
      },
      dispatch,
    ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },

  flatlistItem: {
    backgroundColor: '#fff',
    width: '90%',
    height: h('15%'),
    borderRadius: h('1%'),
    flexDirection: 'row',
    elevation: 5,
  },
  Requst: {
    color: 'white',
    fontSize: h('2%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  btntxt: {
    color: colors.accentBlue,
    fontSize: h('1.8%'),
    marginLeft: h('1%'),
    fontFamily: 'HelveticaNowDisplay-ExtraBold',
  },
  no: {
    color: 'white',
    fontSize: h('4%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  newRequest: {
    backgroundColor: '#fff2',
    width: '80%',
    height: h('15%'),
    flexDirection: 'row',
    borderRadius: h('1%'),
    marginTop: h('4%'),
  },
  left: {
    // backgroundColor: 'green',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    // backgroundColor: 'yellow',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'white',
    width: '95%',
    height: h('5%'),
    borderRadius: h('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: h('2%'),
  },
  flatlistContainer: {
    alignItems: 'center',
    marginTop: h('1.2%'),
    marginBottom: h('1.2%'),
  },
  ImageBackground: {
    backgroundColor: colors.accentBlue,
    width: '100%',
    height: h('30%'),
    alignItems: 'center',
  },
  cardImage: {
    flex: 1,
    width: '100%',
    borderRadius: 8,
  },
  imgtxt: {
    color: 'white',
    fontSize: h('3%'),
    marginTop: h('3%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  flatlistContainerView: {
    backgroundColor: colors.white,
    height: '100%',
    flex: 1,
  },
  leftContainer: {
    // backgroundColor: '#000',
    width: '60%',
    height: '100%',
    justifyContent: 'center',
    padding: h('2%'),
  },
  RightContainer: {
    // backgroundColor: 'red',
    width: '10%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  imgcontainer: {
    backgroundColor: '#ea5455',
    borderRadius: h('100%'),
    width: '80%',
    height: '70%',
  },
  LastContainer: {
    // backgroundColor: 'yellow',
    width: '30%',
    alignItems: 'flex-end',
  },
  nametxt: {
    color: 'black',
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  datetxt: {
    color: colors.primary,
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  descriptiontxt: {
    color: 'gray',
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  addresstxt: {
    color: 'silver',
    fontSize: h('2%'),
    // marginTop: h('1%'),
  },
  noConatiner: {
    // backgroundColor: 'red',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: h('10%'),
    marginTop: h('3%'),
    borderColor: '#ea5455',
    borderWidth: 1,
    marginLeft: h('4%'),
  },
  requestContainr: {
    backgroundColor: '#FF215D',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    paddingLeft: h('2%'),
    // borderRadius: h('10%'),
    marginTop: h('3%'),

    borderTopLeftRadius: h('10%'),
    borderBottomLeftRadius: h('10%'),
  },
  circlebLood: {
    width: '50%',
    height: '42%',
    borderRadius: h('10%'),
    // backgroundColor: 'red',
    marginTop: h('5%'),
    marginRight: h('3%'),
    borderColor: 'silver',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circelTxt: {
    color: '#FF215D',
    fontSize: h('2.5%'),
  },
  requestTxt: {
    color: '#Ffff',
    fontSize: h('1.2%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  frespace: {
    // backgroundColor: 'white',
    width: '100%',
    height: h('5%'),
    // marginTop: -h('50%'),
  },
  labelText: {
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
