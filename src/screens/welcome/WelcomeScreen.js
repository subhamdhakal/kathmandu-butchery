import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {logOut} from '../../actions/fetchdata';
import {AppButton, NavHeader, AppTextinput} from '../../components';
import colors from './../../assets/colors/colors';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

export class WelcomeScreen extends Component {
  render() {
    return (
      <View style={styles.Container}>
        {/* top */}
        <View style={styles.imgContiner}>
          <ImageBackground style={styles.bg}>
            <Image
              style={styles.hand}
              source={require('../../assets/logo.jpg')}
            />
          </ImageBackground>
        </View>
        <View style={styles.loginContainer}>
          <Text style={styles.txt}>Register or Login</Text>
        </View>
        {/* middle */}
        <View style={styles.midleContainer}>
          <View style={styles.emptyContainer}></View>
          <AppButton
            title={'Login'}
            height={'5%'}
            onPress={() => this.props.navigation.navigate('Signin')}
          />
          <AppButton
            title={'Signup'}
            height={'5%'}
            onPress={() => this.props.navigation.navigate('Signup')}
          />
          <View style={styles.Ctext}>
            <Text style={styles.txtc}>मासुमा शुद्धता , हाम्रो प्रतिबद्धता</Text>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        logout,
      },
      dispatch,
    ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imgContiner: {
    // backgroundColor: 'red',
    width: w('100%'),
    height: h('50%'),
  },
  bg: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    marginTop: h('1.5%'),
  },
  login: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginContainer: {
    // backgroundColor: 'red',
    marginTop: -h('13%'),

    width: w('100%'),
    height: h('15%'),
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  hand: {
    resizeMode: 'contain',
    height: h('40%'),
  },
  txt: {
    color: colors.accentBlue,
    fontSize: h('2.5%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  midleContainer: {
    // backgroundColor: 'tomato',
    width: '100%',
    height: h('40%'),
    alignItems: 'center',
  },
  btnContainer: {
    backgroundColor: 'tomato',
    height: h('7%'),
    width: '50%',
    borderRadius: h('7%'),
    overflow: 'hidden',
    marginTop: h('2%'),
  },
  btn: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btntxt: {color: 'white', fontWeight: 'bold', fontSize: h('2.8%')},
  emptyContainer: {
    // backgroundColor: 'tomato',
    height: '20%',
    width: w('100%'),
  },
  Ctext: {
    // backgroundColor: 'red',
    width: '100%',
    height: '42%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  txtc: {
    color: colors.accentBlue,
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 18,
    height: '42%',
  },
});
