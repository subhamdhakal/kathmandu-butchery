/* eslint-disable handle-callback-err */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput} from '../../components';
import AsyncStorage from '@react-native-community/async-storage';
import {login, forgetpassword} from './../../actions/login';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AnimatedLoader from 'react-native-animated-loader';
import {configureNotification} from './../../utils/NotificationConfigure';
import {fetchAllData} from './../../actions/fetchdata';
import colors from '../../assets/colors/colors';
import {NetworkContext} from '../../utils/NetworkProvider';
import NoInternetModal from '../../components/NoInternetModal';

class ForgetPasswordScreen extends Component {
  static contextType = NetworkContext;
  state = {
    email: '',
    password: '',
    loading: false,
  };

  toggleLogin(value) {
    this.setState({
      loading: value,
    });
  }

  validata = () => {
    configureNotification();
    const {email, password} = this.state;
    if (email !== '') {
      this.toggleLogin(true);
      this.props.actions.forgetpassword({
        email: this.state.email,
        onSuccess: ({msg}) => {
          this.toggleLogin(false);
          alert(msg);
        },
        onFailure: (errorMsg) => {
          //Alert error message
          // this.setState({
          //   modalVisible: false,
          // });
          this.toggleLogin(false);
          alert(errorMsg);
        },
      });
    } else {
      alert('Email is Required');
    }
  };

  render() {
    return (
      <View style={styles.Container}>
        <NavHeader
          title={'Forgot Password'}
          onPress={() => this.props.navigation.goBack()}
        />
        <AnimatedLoader
          visible={this.state.loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}></AnimatedLoader>
        {this.context.isConnected ? null : <NoInternetModal />}
        <KeyboardAwareScrollView>
          {/* top */}
          <View style={styles.bgContainer}>
            <Image
              style={styles.hand}
              source={require('../../assets/logo.jpg')}
            />
            <AppTextinput
              name={'Email'}
              onChangeText={(email) => this.setState({email})}
              keyboardType={'email-address'}
            />
            {/* <AppTextinput
              name={'Password'}
              password={true}
              onChangeText={(password) => this.setState({password})}
            /> */}
            {/* <TouchableOpacity style={styles.ftxtContainer}>
              <Text style={styles.ftxt}>Forgot Password ?</Text>
            </TouchableOpacity> */}
            <AppButton
              title={'Reset Password'}
              onPress={() => this.validata()}
            />
          </View>
          {/* bottom */}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        login,
        fetchAllData,
        forgetpassword,
      },
      dispatch,
    ),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgetPasswordScreen);

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
  },
  bgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: h('10%'),
    // backgroundColor: 'red',
    width: w('100%'),
    height: h('70%'),
  },
  ftxtContainer: {
    marginTop: h('2%'),
  },
  ftxt: {
    color: colors.accentBlue,
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  bottomContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  BtoomContainer: {
    // backgroundColor: 'gold',
    height: h('7%'),
    width: '35%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  SContainer: {
    // backgroundColor: 'green',
    height: h('7%'),
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  atxt: {
    color: 'black',
    fontSize: h('1.6%'),

    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  stxt: {
    color: colors.accentBlue,
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  hand: {
    resizeMode: 'contain',
    height: h('30%'),
  },
});
