/* eslint-disable react/self-closing-comp */
/* eslint-disable handle-callback-err */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Modal,
  TextInput,
  TouchableHighlight,
} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  getOrders,
  deletedAllOrderList,
  deletedIndividialOrder,
  updateOrderQuantity,
  updateOrderNotes,
} from '../../database/allSchemas';
import realm from '../../database/allSchemas';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput, ProfilePic} from '../../components';
import AsyncStorage from '@react-native-community/async-storage';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {AnimatedFlatList, AnimationType} from 'flatlist-intro-animations';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Avatar} from 'react-native-paper';
import colors from '../../assets/colors/colors';
import AnimatedLoader from 'react-native-animated-loader';
import Toast from 'react-native-simple-toast';

import {
  fetcheduserrequestlist,
  placeorder,
  fetchAllData,
} from '../../actions/fetchdata';
import {color} from 'react-native-reanimated';
import RegularText from './../../components/RegularText';

export class MyCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartCount: 0,
      orderList: [],
      usercredentialsModal: false,
      total: 0,
      name: '',
      address: '',
      phone: '',
      orderToPlace: [],
      loading: false,
      notesModal: false,
      selectedItem: {},
      updatedNotes: '',
    };
    this.reloadData();
    realm.addListener('change', () => {
      this.reloadData();
    });
  }
  calculateTotal = () => {
    this.state.orderList.map((item) => {
      console.log('item' + JSON.stringify(item));
      const orderToPlace = {
        id: item.id,
        title: item.title,
        quantity: item.quantity,
        price: item.price,
        image: item.imageUrl,
        note: item.notes,
      };
      this.state.orderToPlace.push(orderToPlace);
    });
    console.log(JSON.stringify(this.state.orderToPlace));
    this.checkout();
  };
  sumOfItems = () => {
    var totalOrderPrice = 0;
    this.state.orderList.map((item) => {
      totalOrderPrice = totalOrderPrice + parseInt(item.totalPrice);
      console.log(totalOrderPrice);
    });
    this.setState({
      total: totalOrderPrice,
    });
  };
  reloadData = () => {
    getOrders()
      .then((orderList) => {
        this.setState({orderList});

        console.log('Here are orders' + JSON.stringify(orderList));
        this.sumOfItems();
      })
      .catch((error) => {
        alert(`Insert new ${error}`);
      });
  };
  RenderItem = ({item}) => (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: h('1%'),
        margin: h('1%'),
        elevation: 5,
        padding: h('2%'),
      }}>
      <View style={{flex: 0.3, borderRadius: 8}}>
        <Image
          style={{
            width: 72,
            height: 72,
            borderRadius: 8,
            overflow: 'hidden',
          }}
          source={{uri: item.imageUrl}}
          resizeMode={'center'} // <- needs to be "cover" for borderRadius to take effect on Android
        />
      </View>
      <View style={{flex: 0.3, flexDirection: 'column'}}>
        <Text
          style={{
            fontSize: 12,
            color: colors.brown,
            fontFamily: 'HelveticaNowDisplay-Bold',
          }}>
          {item.title}
        </Text>
        <Text
          style={{
            fontSize: 16,
            fontFamily: 'HelveticaNowDisplay-Bold',
            color: colors.darkGreen,
          }}>
          $ {item.price}
        </Text>
        {item.notes === '' ? null : (
          <Text
            style={{
              fontSize: 12,
              fontFamily: 'HelveticaNowDisplay-Bold',
              color: colors.accentBlue,
            }}>
            Note: {item.notes}
          </Text>
        )}
      </View>
      <View style={{flex: 0.2, margin: 24}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            style={{margin: 12}}
            onPress={() =>
              this.updateOrderQuantityDecrease(
                item.id,
                item.quantity,
                item.price,
              )
            }>
            <AntDesign
              name="minuscircleo"
              color={colors.accentBlue}
              style={{
                backgroundColor: colors.darkGreen,
                borderRadius: 4,
              }}
              size={18}
            />
          </TouchableOpacity>

          <Text
            style={{
              color: colors.dark,
              fontFamily: 'HelveticaNowDisplay-Regular',
            }}>
            {item.quantity}
          </Text>
          <TouchableOpacity
            style={{margin: 12}}
            onPress={() =>
              this.updateOrderQuantity(item.id, item.quantity, item.price)
            }>
            <AntDesign
              name="pluscircleo"
              color={colors.accentBlue}
              style={{
                backgroundColor: colors.darkGreen,
                borderRadius: 4,
              }}
              size={18}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          flex: 0.2,
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginLeft: 20,
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{margin: 8}}
            onPress={() =>
              this.setState({notesModal: true, selectedItem: item})
            }>
            <AntDesign
              name="form"
              color={colors.accentBlue}
              style={{
                backgroundColor: colors.darkGreen,
                borderRadius: 4,
              }}
              size={18}
            />

            {/* <Text
              style={{
                fontSize: 12,
                color: colors.brown,
                fontFamily: 'HelveticaNowDisplay-Bold',
              }}>
              {item.notes === '' ? 'Add note' : 'Update note'}
            </Text> */}
          </TouchableOpacity>
          <TouchableOpacity
            style={{margin: 8}}
            onPress={() => this.performDeletion(item.id)}>
            <Icon name="delete-outline" color={colors.rejectRed} size={24} />
          </TouchableOpacity>
          <RegularText label={'$ ' + item.totalPrice} />
        </View>
      </View>
    </View>
  );
  placeOrder = () => {
    this.setState({
      loading: true,
    });
    this.props.actions.placeorder({
      totalAmount: this.state.total,
      orderList: this.state.orderToPlace,
      accessToken: this.props.access_token,
      onSuccess: () => {
        this.setState({
          loading: false,
          orderToPlace: [],
        });
        this.reloadData();
        this.props.actions.fetchAllData({
          accessToken: this.props.access_token,
          onSuccess: () => {
            this.setState({
              selectedItem: {},
            });
            deletedAllOrderList()
              .then(
                Toast.show('Order placed successfully!'),
                this.props.navigation.navigate('Events'),
              )
              .catch((error) => {
                alert(error);
              });
          },
          onFailure: (errorMsg) => {
            alert(errorMsg);
          },
        });
      },
      onFailure: () => {
        //Alert error message
      },
    });
  };
  hideModal = () => this.setState({usercredentialsModal: false});
  performDeletion = (id) => {
    deletedIndividialOrder(id)
      .then()
      .catch((error) => {
        alert(`delete order ${error}`);
      });
  };
  updateOrderQuantity = (id, quantity, price) => {
    updateOrderQuantity(id, quantity + 1, (quantity + 1) * price)
      .then()
      .catch((error) => {
        alert(`update quantity ${error}`);
      });
  };
  updateOrderQuantityDecrease = (id, quantity, price) => {
    if (quantity == 1) {
      this.performDeletion(id);
    } else {
      updateOrderQuantity(id, quantity - 1, (quantity - 1) * price)
        .then()
        .catch((error) => {
          alert(`update quantity ${error}`);
        });
    }
  };
  checkout = () => {
    Alert.alert('Checkout', 'Are you sure you want to checkout ?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
        onPress: () =>
          this.state.orderToPlace.length != 0
            ? this.placeOrder()
            : alert('Please add items in cart to place order.'),
      },
    ]);
  };
  render() {
    return (
      <View style={styles.Container}>
        <ImageBackground style={styles.ImageBackground}>
          <AnimatedLoader
            visible={this.state.loading}
            overlayColor="rgba(255,255,255,0.75)"
            source={require('../../assets/loader.json')}
            animationStyle={styles.lottie}
            speed={1}></AnimatedLoader>

          <Modal
            transparent={true}
            visible={this.state.notesModal}
            onRequestClose={() => this.setState({notesModal: false})}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View
                  style={{
                    alignSelf: 'flex-end',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => this.setState({notesModal: false})}>
                    <AntDesign
                      name="closecircleo"
                      color={colors.accentBlue}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
                <Text
                  style={{
                    fontFamily: 'HelveticaNowDisplay-ExtraBold',
                    fontSize: 18,
                    padding: 8,
                    marginLeft: 24,
                    marginRight: 24,
                  }}></Text>
                <Text
                  style={{
                    fontFamily: 'HelveticaNowDisplay-ExtraBold',
                    fontSize: 10,
                  }}>
                  ENTER NOTES:
                </Text>
                <Text
                  style={{
                    fontFamily: 'HelveticaNowDisplay-ExtraBold',
                    fontSize: 10,
                    color: colors.accentBlue,
                  }}>
                  {this.state.selectedItem.title}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    padding: 8,

                    alignItems: 'center',
                  }}>
                  <TextInput
                    style={{
                      backgroundColor: '#0001',
                      width: w('60%'),
                      height: h('16%'),
                      borderRadius: h('2%'),
                      paddingLeft: h('3%'),
                    }}
                    multiline
                    numberOfLines={2}
                    defaultValue={this.state.selectedItem.notes}
                    keyboardType={'default'}
                    onChangeText={(notes) =>
                      this.setState({updatedNotes: notes})
                    }
                  />
                </View>
                <TouchableOpacity
                  onPress={() => {
                    updateOrderNotes(
                      this.state.selectedItem.id,
                      this.state.updatedNotes,
                    )
                      .then()
                      .catch((error) => {
                        alert(`update notes ${error}`);
                      }),
                      this.setState({
                        notesModal: false,
                      });
                  }}
                  style={{
                    backgroundColor: colors.purple,
                    borderRadius: 8,
                    margin: 8,
                    marginBottom: 16,
                  }}>
                  <Text
                    style={{
                      paddingLeft: 24,
                      paddingRight: 24,
                      paddingTop: 12,
                      paddingBottom: 12,
                      color: colors.accentBlue,
                      fontFamily: 'HelveticaNowDisplay-ExtraBold',
                    }}>
                    Done
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Text style={styles.imgtxt}>My Cart</Text>
          <View style={styles.newRequest}>
            <View style={styles.left}>
              <Text style={styles.no}>$ {this.state.total}</Text>
              <Text style={styles.Requst}>Total Amount</Text>
            </View>
            <View style={styles.right}>
              <TouchableOpacity
                onPress={() => {
                  this.calculateTotal();
                }}
                style={styles.btn}>
                <AntDesign
                  name="shoppingcart"
                  color={colors.accentBlue}
                  size={20}
                />
                <Text style={styles.btntxt}>Checkout</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* flat */}
        </ImageBackground>
        <View style={styles.flatlistContainerView}>
          <AnimatedFlatList
            contentContainerStyle={{marginTop: h('1%')}}
            data={this.state.orderList}
            renderItem={this.RenderItem}
            animationType={AnimationType.SlideFromRight}
            keyExtractor={(item) => item.title}
            animationDuration={1000}
            focused={true}
          />
        </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    mybloodrequest: state.dataReducer.bloodRequest,
    userData: state.loginReducer.loginResponse['user'],
    access_token: state.loginReducer.loginResponse['token'],
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        fetcheduserrequestlist,
        placeorder,
        fetchAllData,
      },
      dispatch,
    ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCart);
const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  ImageBackground: {
    backgroundColor: colors.accentBlue,
    width: '100%',
    height: h('30%'),
    alignItems: 'center',
  },
  imgtxt: {
    color: 'white',
    fontSize: h('3%'),
    marginTop: h('3%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  newRequest: {
    backgroundColor: '#fff2',
    width: '80%',
    height: h('15%'),
    flexDirection: 'row',
    borderRadius: h('1%'),
    marginTop: h('4%'),
  },
  left: {
    // backgroundColor: 'green',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    // backgroundColor: 'yellow',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'white',
    width: '95%',
    height: h('5%'),
    borderRadius: h('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: h('2%'),
  },
  btntxt: {
    color: colors.accentBlue,
    fontSize: h('1.8%'),
    marginLeft: h('1%'),
    fontFamily: 'HelveticaNowDisplay-ExtraBold',
  },
  no: {
    color: 'white',
    fontSize: h('4%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  Requst: {
    color: 'white',
    fontSize: h('2%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  flatlistItem: {
    backgroundColor: '#fff',
    width: '90%',
    height: h('15%'),
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: h('1%'),
    marginTop: h('1%'),
  },
  flatlistContainer: {
    alignItems: 'center',
    marginTop: h('1.2%'),
  },
  flatlistContainerView: {
    backgroundColor: colors.white,
    height: '100%',
    flex: 1,
  },
  leftContainer: {
    // backgroundColor: '#000',
    width: '20%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  RightContainer: {
    // backgroundColor: 'red',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
  },
  imgcontainer: {
    backgroundColor: '#ea5455',
    borderRadius: h('100%'),
    width: '80%',
    height: '70%',
  },
  LastContainer: {
    width: '30%',
    alignItems: 'flex-end',
  },
  nametxt: {
    color: 'black',
    fontSize: h('2%'),
    marginTop: h('3%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  addresstxt: {
    color: 'silver',
    fontSize: h('2%'),
    // marginTop: h('1%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  noConatiner: {
    // backgroundColor: 'red',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: h('10%'),
    marginTop: h('3%'),
    borderColor: '#ea5455',
    borderWidth: 1,
  },
  requestContainr: {
    backgroundColor: '#FF215D',
    width: '75%',
    height: '25%',
    justifyContent: 'center',
    paddingLeft: h('1%'),
    // borderRadius: h('10%'),
    marginTop: h('3%'),

    borderTopLeftRadius: h('10%'),
    borderBottomLeftRadius: h('10%'),
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: h('2%'),
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  circlebLood: {
    width: '30%',
    height: '30%',
    borderRadius: h('10%'),
    // backgroundColor: 'red',
    marginTop: h('2%'),
    marginRight: h('3%'),
    borderColor: 'silver',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circelTxt: {
    color: '#FF215D',
    fontSize: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  requestTxt: {
    color: '#Ffff',
    fontSize: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  frespace: {
    backgroundColor: 'white',
    width: '100%',
    height: h('35.1%'),
    // marginTop: -h('50%'),
  },
  labelText: {
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 10,
    alignSelf: 'center',
    alignItems: 'center',
  },
});
