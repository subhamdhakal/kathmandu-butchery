/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-sparse-arrays */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import AnimatedLoader from 'react-native-animated-loader';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput} from '../../components';
var validator = require('email-validator');
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {updateuser} from './../../actions/signup';

class EditProfile extends Component {
  state = {
    name: undefined,
    email: undefined,
    phone: undefined,
    address: undefined,
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.setState({
      name: this.props.userData['name'],
      email: this.props.userData['email'],
      phone: this.props.userData['mobile'],
      address: this.props.userData['address'],
    });
  }

  toggleLogin(value) {
    this.setState({
      loading: value,
    });
  }

  validate = () => {
    const {name, email, phone, address} = this.state;
    console.log('name' + this.state.name, this.state.email);
    const check = validator.validate(this.state.email.replace(/\s/g, ''));
    if (name !== '') {
      if (phone !== '') {
        if (address != '') {
          const value = {
            full_name: this.state.name,
            mobile: this.state.phone,
            address: this.state.address,
          };
          this.toggleLogin(true);

          this.props.actions.updateuser({
            accessToken: this.props.access_token,
            updateDetails: value,

            onSuccess: () => {
              this.toggleLogin(false);
              alert('Profile Updated Successful');
            },
            onFailure: (errorMsg) => {
              this.toggleLogin(false);
              alert(errorMsg);

              //Alert error message
            },
          });
        } else {
          alert('Address is required');
        }
      } else {
        alert('Phone is required');
      }
    } else {
      alert('Name is Required');
    }
  };

  setDate = (event, date) => {
    if (date !== undefined) {
      // timeSetAction
      console.log(date);
      this.setState({
        dateofbirth: moment(date).format('YYYY-MM-DD').toString(),
        showDatePicker: false,
      });
    }
  };

  setLastDonatedDate = (event, date) => {
    if (date !== undefined) {
      // timeSetAction
      console.log('last donated date' + date);
      this.setState({
        last_blood_donated: moment(date).format('YYYY-MM-DD').toString(),
        showLastDonatedDatePicker: false,
      });
    }
  };
  showDatepicker = () => {
    this.setState({
      showDatePicker: true,
    });
  };
  _selectedValueMaterial(index, item) {
    this.setState({district: item.name});
  }

  render() {
    return (
      <View style={styles.Container}>
        <NavHeader
          title={'Signup'}
          onPress={() => this.props.navigation.goBack()}
        />
        <AnimatedLoader
          visible={this.state.loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}></AnimatedLoader>
        {/* top */}
        <AnimatedLoader
          visible={this.state.loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}></AnimatedLoader>
        <KeyboardAwareScrollView>
          <View style={styles.topContainer}>
            <View style={styles.txtinputContainer}>
              <AppTextinput
                name={'Full Name'}
                onChangeText={(name) => this.setState({name})}
                defaultValue={this.props.userData['name']}
              />
              <AppTextinput
                name={'Phone'}
                onChangeText={(phone) => this.setState({phone})}
                defaultValue={this.props.userData['mobile']}
                keyboardType={'numeric'}
              />
              <AppTextinput
                name={'Address'}
                onChangeText={(address) => this.setState({address})}
                defaultValue={this.props.userData['address']}
              />
            </View>
          </View>

          <View style={styles.btnview}>
            <AppButton title={'Update'} onPress={() => this.validate()} />

            <View style={{flexDirection: 'row', marginTop: h('2%')}}>
              <View
                style={{
                  // backgroundColor: 'green',
                  width: '35%',
                  height: h('5%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}></View>
              <TouchableOpacity
                onPress={() => this.props.navigation.replace('Signin')}
                style={{
                  // backgroundColor: 'green',
                  width: '15%',
                  height: h('5%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}></TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userData: state.loginReducer.loginResponse['user'],
    access_token: state.loginReducer.loginResponse['token'],
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        updateuser,
      },
      dispatch,
    ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
  },
  topContainer: {
    // backgroundColor: 'red',
    width: '100%',
    height: h('60%'),
  },
  lottie: {
    width: 200,
    height: 200,
  },
  midContainer: {
    // backgroundColor: 'yellow',
    width: '100%',
    height: h('20%'),
  },
  botmContainer: {
    // backgroundColor: 'tomato',
    width: '100%',
    height: h('30%'),
  },
  gtxt: {
    fontSize: h('2.5%'),
    color: 'black',
    marginLeft: h('7%'),
    marginTop: h('1%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  txtinputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 16,
  },
  imgContainer: {
    flexDirection: 'row',
    // backgroundColor: 'red',

    height: h('15%'),
  },
  img: {
    width: '50%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightColor: 'rgba(0,0,0,0.2)',
    borderRightWidth: h('0.1%'),
  },
  imgr: {
    width: '30%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightColor: 'rgba(0,0,0,0.2)',
    borderRightWidth: h('0.1%'),
  },
  img2: {
    width: '30%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftColor: 'rgba(0,0,0,0.2)',
    borderLeftWidth: h('0.1%'),
  },
  imgs: {
    height: h('8%'),
    width: '100%',
    resizeMode: 'contain',
  },
  txtf: {
    fontSize: h('1.5%'),
    marginTop: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  txtm: {
    color: '#000',
    marginTop: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  BotomContainerView: {
    width: '100%',
    height: '100%',
    // backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  circle: {
    // backgroundColor: 'yellow',
    width: '10%',
    height: '20%',
    borderRadius: h('10%'),
    marginTop: h('3%'),
    marginLeft: h('2%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: h('0.3%'),
    borderColor: 'rgba(0,0,0,0.3)',
  },
  txta: {
    fontSize: h('1.6%'),
    fontFamily: 'HelveticaNowDisplay-Bold',

    // color: this.state.blood !== '' ? 'white' : 'black',
  },
  BottomContainerView: {
    width: '100%',
    height: '100%',
    // backgroundColor: 'red',
    flexDirection: 'row',
    marginTop: -h('20%'),
    marginLeft: h('6%'),
    // justifyContent: 'center',
  },
  btnview: {
    // backgroundColor: 'red',
    width: '100%',
    height: h('20%'),
    alignItems: 'center',
    marginTop: -h('5%'),
  },
});
