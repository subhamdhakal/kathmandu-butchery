/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-sparse-arrays */
import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {NetworkContext} from '../../utils/NetworkProvider';
import NoInternetModal from '../../components/NoInternetModal';
import moment from 'moment';
import AnimatedLoader from 'react-native-animated-loader';

import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from '@codler/react-native-keyboard-aware-scroll-view';
import {AppButton, NavHeader, AppTextinput} from '../../components';
var validator = require('email-validator');
import AsyncStorage from '@react-native-community/async-storage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {signup} from './../../actions/signup';
import colors from '../../assets/colors/colors';
import LabelText from './../../components/LabelText';

class Signup extends Component {
  static contextType = NetworkContext;

  state = {
    name: '',
    email: '',
    phone: '',
    password: '',
    confirmpassword: '',
    gender: '',
    blood: '',
    showDatePicker: false,
    showLastDonatedDatePicker: false,
    dateofbirth: '',
    disease: '',
    address: '',
    location: '',
    role: '',
    address: '',
    loading: false,
    last_blood_donated: '',
  };
  constructor(props) {
    super(props);

    const person = {
      id: '',
      name: '',
    };
  }

  componentDidMount() {}

  toggleLogin(value) {
    this.setState({
      loading: value,
    });
  }

  validate = () => {
    const {name, email, phone, password, confirmpassword, address} = this.state;
    const check = validator.validate(this.state.email.replace(/\s/g, ''));
    if (check === true) {
      if (password !== '') {
        if (password.length > 8) {
          if (name !== '') {
            if (phone !== '') {
              if (confirmpassword !== '') {
                if (password === confirmpassword) {
                  if (address != '') {
                    const formData = new FormData();
                    formData.append('name', this.state.name);
                    formData.append(
                      'email',
                      this.state.email.replace(/\s/g, ''),
                    );
                    formData.append('mobile', this.state.phone);
                    formData.append('address', this.state.address);
                    formData.append('password', this.state.password);
                    formData.append(
                      'password_confirmation',
                      this.state.confirmpassword,
                    );
                    this.toggleLogin(true);
                    this.props.actions.signup({
                      signUpDetails: formData,

                      onSuccess: () => {
                        this.toggleLogin(false);
                        alert('Sign Up successful.Please Sign in!');
                        this.props.navigation.replace('Signin');
                      },
                      onFailure: (errorMsg) => {
                        this.toggleLogin(false);
                        alert(errorMsg);

                        //Alert error message
                      },
                    });
                  } else {
                    alert('Address is required');
                  }
                } else {
                  alert('Password and Cofirm password is not same');
                }
              } else {
                alert('Confirm password is Required');
              }
            } else {
              alert('Phone No is Required');
            }
          } else {
            alert('Name is Required');
          }
        } else {
          alert('Password should be of 8 Characters');
        }
      } else {
        alert('password is Required');
      }
    } else {
      alert('email is incorect');
    }
  };

  setDate = (event, date) => {
    if (date !== undefined) {
      // timeSetAction
      console.log(date);
      this.setState({
        dateofbirth: moment(date).format('YYYY-MM-DD').toString(),
        showDatePicker: false,
      });
    }
  };

  setLastDonatedDate = (event, date) => {
    if (date !== undefined) {
      // timeSetAction
      console.log('last donated date' + date);
      this.setState({
        last_blood_donated: moment(date).format('YYYY-MM-DD').toString(),
        showLastDonatedDatePicker: false,
      });
    }
  };
  showDatepicker = () => {
    this.setState({
      showDatePicker: true,
    });
  };
  _selectedValueMaterial(index, item) {
    this.setState({district: item.name});
  }

  render() {
    const date = new Date();
    const lastDonated = new Date();

    return (
      <View style={styles.Container}>
        <NavHeader
          title={'Signup'}
          onPress={() => this.props.navigation.goBack()}
        />
        {this.context.isConnected ? null : <NoInternetModal />}

        <AnimatedLoader
          visible={this.state.loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}></AnimatedLoader>
        {/* top */}
        <AnimatedLoader
          visible={this.state.loading}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../assets/loader.json')}
          animationStyle={styles.lottie}
          speed={1}></AnimatedLoader>
        <KeyboardAwareScrollView>
          <View style={styles.topContainer}>
            <View style={styles.txtinputContainer}>
              <AppTextinput
                name={'Full Name'}
                onChangeText={(name) => this.setState({name})}
              />
              <AppTextinput
                name={'Email'}
                onChangeText={(email) => this.setState({email})}
                keyboardType={'email-address'}
              />
              <AppTextinput
                name={'Phone'}
                onChangeText={(phone) => this.setState({phone})}
                keyboardType={'numeric'}
              />
              <AppTextinput
                name={'Address'}
                onChangeText={(address) => this.setState({address})}
              />

              <AppTextinput
                name={'Password'}
                password={true}
                onChangeText={(password) => this.setState({password})}
              />
              <AppTextinput
                name={'Confirm Password'}
                password={true}
                onChangeText={(confirmpassword) =>
                  this.setState({confirmpassword})
                }
              />
            </View>
          </View>

          <View style={styles.btnview}>
            <AppButton title={'Signup'} onPress={() => this.validate()} />

            <View style={{flexDirection: 'row', marginTop: h('2%')}}>
              <View
                style={{
                  // backgroundColor: 'green',
                  width: '35%',
                  height: h('5%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'black'}}>Already a member ?</Text>
              </View>
              <TouchableOpacity
                onPress={() => this.props.navigation.replace('Signin')}
                style={{
                  // backgroundColor: 'green',
                  width: '15%',
                  height: h('5%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: colors.accentBlue,
                  }}>
                  Sign In
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        signup,
      },
      dispatch,
    ),
  };
}

export default connect(null, mapDispatchToProps)(Signup);
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'white',
  },
  topContainer: {
    // backgroundColor: 'red',
    width: '100%',
    height: h('60%'),
  },

  botmContainer: {
    // backgroundColor: 'tomato',
    width: '100%',
    height: h('40%'),
  },
  gtxt: {
    fontSize: h('2.5%'),
    color: 'black',
    marginLeft: h('7%'),
    marginTop: h('1%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  txtinputContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 16,
  },
  imgContainer: {
    flexDirection: 'row',
    // backgroundColor: 'red',

    height: h('15%'),
  },
  img: {
    width: '50%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightColor: 'rgba(0,0,0,0.2)',
    borderRightWidth: h('0.1%'),
  },
  imgr: {
    width: '30%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightColor: 'rgba(0,0,0,0.2)',
    borderRightWidth: h('0.1%'),
  },
  img2: {
    width: '30%',
    height: '100%',
    // backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftColor: 'rgba(0,0,0,0.2)',
    borderLeftWidth: h('0.1%'),
  },
  imgs: {
    height: h('8%'),
    width: '100%',
    resizeMode: 'contain',
  },
  txtf: {
    fontSize: h('1.5%'),
    marginTop: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  txtm: {
    color: '#000',
    marginTop: h('1.5%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  BotomContainerView: {
    width: '100%',
    height: '100%',
    // backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  circle: {
    // backgroundColor: 'yellow',
    width: '10%',
    height: '20%',
    borderRadius: h('10%'),
    marginTop: h('3%'),
    marginLeft: h('2%'),
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: h('0.3%'),
    borderColor: 'rgba(0,0,0,0.3)',
  },
  txta: {
    fontSize: h('1.6%'),
    fontFamily: 'HelveticaNowDisplay-Bold',

    // color: this.state.blood !== '' ? 'white' : 'black',
  },
  BottomContainerView: {
    width: '100%',
    height: '100%',
    // backgroundColor: 'red',
    flexDirection: 'row',
    marginTop: -h('20%'),
    marginLeft: h('6%'),
    // justifyContent: 'center',
  },
  btnview: {
    // backgroundColor: 'red',
    width: '100%',
    height: h('20%'),
    alignItems: 'center',
    marginTop: h('2%'),
  },
});
