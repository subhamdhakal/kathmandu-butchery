import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const Stack = createStackNavigator();

// screens
import {WelcomeScreen} from '../screens/welcome';

import {SplitScreen} from '../screens/split';
import Dashboard from '../screens/dasboard/Dashboard';
import {navigationRef} from './../services/NavigationService';

// navigators
import {BottomTab} from './BottomTab';
import Signin from './../screens/signin/Signin';
import Signup from './../screens/signup/Signup';
import EditProfile from '../screens/profile/EditProfile';
import ChangePassword from './../screens/profile/ChangePassword';
import ForgetPasswordScreen from './../screens/forgetpassword/ForgetPasswordScreen';

export class StackNavigator extends Component {
  render() {
    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator>
          <Stack.Screen
            name="SplitScreen"
            component={SplitScreen}
            options={{headerShown: false}}
          />

          <Stack.Screen
            name="WelcomeScreen"
            component={WelcomeScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Signin"
            component={Signin}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Signup"
            component={Signup}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="BottomTab"
            component={BottomTab}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ForgetPasswordScreen"
            component={ForgetPasswordScreen}
            options={{headerShown: false}}
          />

          <Stack.Screen
            name="EditProfile"
            component={EditProfile}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ChangePassword"
            component={ChangePassword}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
