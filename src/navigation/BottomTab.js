import React, {Component} from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
const Tab = createMaterialBottomTabNavigator();
import AntDesign from 'react-native-vector-icons/MaterialCommunityIcons';

// screen
import Dashboard from '../screens/dasboard/Dashboard';
import MyCart from '../screens/cart/MyCart';
import ProfileScreen from '../screens/profile/ProfileScreen';
import OrderHistory from './../screens/orderhistory/OrderHistory';
import colors from './../../src/assets/colors/colors';

export class BottomTab extends Component {
  render() {
    return (
      <Tab.Navigator
        initialRouteName="Home"
        activeColor={colors.accentBlue}
        inactiveColor="gray"
        barStyle={{backgroundColor: '#fff'}}>
        <Tab.Screen
          name="Feed"
          component={Dashboard}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color}) => (
              <AntDesign name="home" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="MyCart"
          component={MyCart}
          options={{
            tabBarLabel: 'Cart',
            tabBarIcon: ({color}) => (
              <AntDesign name="cart" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Events"
          component={OrderHistory}
          options={{
            tabBarLabel: 'Order History',
            tabBarIcon: ({color}) => (
              <AntDesign name="history" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="ProfileScreen"
          component={ProfileScreen}
          options={{
            tabBarLabel: 'Account',
            tabBarIcon: ({color}) => (
              <AntDesign name="account" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    );
  }
}
