import {
  PRODUCT_LIST_FETCHED,
  BLOOD_REQUEST_FETCHED,
  EVENT_LIST_FETCHED,
  USER_MY_REQUEST_FETCHED,
  FETCHED_ORDER_HISTORY,
  LOG_OUT,
} from '../constants/action-types';

const initialState = {
  productList: [],
  bloodRequest: [],
  orderHistory: [],
  userRequest: [],
};

function dataReducer(state = initialState, action) {
  if (action.type === PRODUCT_LIST_FETCHED) {
    console.log('here is reducer' + action.payload);
    return {
      ...state,
      productList: action.payload,
    };
  }
  if (action.type === BLOOD_REQUEST_FETCHED) {
    return {
      ...state,
      bloodRequest: action.payload,
    };
  }
  if (action.type === FETCHED_ORDER_HISTORY) {
    return {
      ...state,
      orderHistory: action.payload,
    };
  }
  if (action.type === USER_MY_REQUEST_FETCHED) {
    return {
      ...state,
      userRequest: action.payload,
    };
  }
  if (action.type === LOG_OUT) {
    return {
      state: initialState,
    };
  }

  return state;
}

export const fetchedProductList = (data) => {
  return {
    type: PRODUCT_LIST_FETCHED,
    payload: data,
  };
};

export const fetchedOrderHistroy = (data) => {
  return {
    type: FETCHED_ORDER_HISTORY,
    payload: data,
  };
};

export const fetchedEventList = (data) => {
  return {
    type: EVENT_LIST_FETCHED,
    payload: data,
  };
};
export const fetchedUserRequest = (data) => {
  return {
    type: USER_MY_REQUEST_FETCHED,
    payload: data,
  };
};
export const handleLogOut = () => {
  return {
    type: LOG_OUT,
  };
};

export default dataReducer;
