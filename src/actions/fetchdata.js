import {BASE_URL} from './../constants/app-constants';
import axios from 'react-native-axios';
const queryString = require('query-string');
import {
  fetchedUserRequest,
  fetchedProductList,
  fetchedOrderHistroy,
  handleLogOut,
} from '../reducers/dataReducer';

export const fetchAllData = ({accessToken, onSuccess, onFailure}) => {
  console.log('on fetch all data');
  const productlist = axios.get(BASE_URL + '/api/v1/product-list');

  const orderHistory = axios.get(BASE_URL + 'api/v1/user-order-history', {
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  // const eventlist = axios.get(BASE_URL + 'api/v1/get-event', {
  //   headers: {
  //     Authorization: `Bearer ${accessToken}`,
  //   },
  // });

  return (dispatch) => {
    return axios
      .all([productlist, orderHistory])
      .then(
        axios.spread((productListResponse, orderHistoryResponse) => {
          console.log(JSON.stringify(orderHistoryResponse));
          dispatch(fetchedProductList(productListResponse['data']['data']));
          dispatch(fetchedOrderHistroy(orderHistoryResponse['data']['data']));
          onSuccess();
        }),
      )

      .catch((err) => {
        console.log(err);
        onFailure();
      });
  };
};

export const fetcheduserrequestlist = ({
  accessToken,
  user_id,
  onSuccess,
  onFailure,
}) => {
  return (dispatch) => {
    return axios
      .get(BASE_URL + 'api/v1/blood-request-list?user_id=' + user_id, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((res) => {
        console.log('individual' + JSON.stringify(res['data']['data']));
        dispatch(fetchedUserRequest(res.data['data']));
        onSuccess();
      })
      .catch((err) => {
        console.log(err);
        onFailure();
      });
  };
};

export const placeorder = ({
  totalAmount,
  orderList,
  accessToken,
  onSuccess,
  onFailure,
}) => {
  return (dispatch) => {
    return axios
      .post(
        BASE_URL + 'api/v1/order-post',
        {
          amount: totalAmount,
          products: orderList,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
          },
        },
      )
      .then((res) => {
        // dispatch(fetchedUserRequest(res.data['data']));
        onSuccess();
      })
      .catch((err) => {
        console.log(JSON.stringify(err));
        onFailure();
      });
  };
};

export const deleteorder = ({orderId, accessToken, onSuccess, onFailure}) => {
  return (dispatch) => {
    return axios
      .post(
        BASE_URL + 'api/v1/user-order-cancel',
        {
          order_id: orderId,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
          },
        },
      )
      .then((res) => {
        console.log(JSON.stringify(res));
        onSuccess();
      })
      .catch((err) => {
        console.log(JSON.stringify(err));
        onFailure();
      });
  };
};
export const logOut = () => {
  return (dispatch) => {
    dispatch(handleLogOut());
  };
};
