import {BASE_URL} from './../constants/app-constants';
import axios from 'react-native-axios';
const queryString = require('query-string');
import {loginSuccessful} from '../reducers/loginReducer';
import AsyncStorage from '@react-native-community/async-storage';

export const login = ({email, password, onSuccess, onFailure}) => {
  console.log('login pressed' + email + password);
  return (dispatch) => {
    return axios
      .post(BASE_URL + 'api/v1/login', {
        email: email,
        password: password,
      })
      .then((res) => {
        dispatch(loginSuccessful(res.data));
        try {
          AsyncStorage.setItem(
            'userdata',
            JSON.stringify(res.data.user),
            () => {
              onSuccess({accessToken: res.data.token});
            },
          );
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(JSON.stringify(err));
        onFailure(err.response.data.error);
      });
  };
};

export const forgetpassword = ({email, onSuccess, onFailure}) => {
  return (dispatch) => {
    return axios
      .post(BASE_URL + 'api/v1/forgot-password', {
        email: email,
      })
      .then((res) => {
        console.log(res.data.message.msg);
        onSuccess({msg: res.data.message.msg});
      })
      .catch((err) => {
        console.log(JSON.stringify(err));
        onFailure(err.response.data.error);
      });
  };
};
