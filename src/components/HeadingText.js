import React from 'react';
import colors from '../assets/colors/colors';
import {Text} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';

const HeadingText = ({label}) => {
  return (
    <Text
      style={{
        fontSize: h('2%'),
        color: colors.simrik,
        fontFamily: 'HelveticaNowDisplay-ExtraBold',
        alignSelf: 'center',
      }}>
      {label}
    </Text>
  );
};

export default HeadingText;
