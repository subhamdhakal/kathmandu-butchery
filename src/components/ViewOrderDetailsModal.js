import * as React from 'react';
import {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';
import {AnimatedFlatList, AnimationType} from 'flatlist-intro-animations';
import {Modal, Portal, Text, Button, Provider} from 'react-native-paper';
import {AppTextinput} from './AppTextinput';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';

import {
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import colors from '../assets/colors/colors';

export default class ViewOrderDetailsModal extends Component {
  state = {
    hideModal: true,
    cancelReason: '',
  };
  RenderItem = (item) => (
    <View style={styles.flatlistContainer}>
      <View style={styles.flatlistItem}>
        <View style={styles.leftContainer}>
          <Image
            style={{
              width: 72,
              height: 72,
              borderRadius: 8,
            }}
            source={{uri: item.image}}
            resizeMode={'center'}
          />
        </View>
        <View style={styles.RightContainer}>
          <Text style={styles.datetxt}>{item.product_title}</Text>
          <Text style={styles.nametxt}>{'Price: $ ' + item.price}</Text>
          <Text style={styles.nametxt}>{'Quantity: ' + item.quantity}</Text>
          <Text style={styles.nametxt}>
            {'Total Price: $ ' + item.quantity * item.price}
          </Text>
        </View>
      </View>
    </View>
  );

  render() {
    return (
      <Provider {...this.props}>
        <Portal>
          <Modal
            visible={this.props.visible}
            transparent={true}
            onDismiss={() => this.props.dismissCancelModal()}
            contentContainerStyle={{
              backgroundColor: 'white',
              borderRadius: 20,
              shadowColor: '#000',
              alignSelf: 'center',
              width: '80%',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              height: '60%',
              padding: 4,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                alignSelf: 'flex-end',
              }}>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => this.props.dismissCancelModal()}>
                <AntDesign
                  name="closecircleo"
                  color={colors.accentBlue}
                  size={20}
                />
              </TouchableOpacity>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.datetxt}>
                {'Order Date: ' + this.props.orderDetails.order_date}
              </Text>
              <Text style={styles.nametxt}>
                {'Total Amount: $ ' + this.props.orderDetails.total_amount}
              </Text>
            </View>

            <AnimatedFlatList
              contentContainerStyle={{
                marginTop: -h('1%'),
              }}
              data={this.props.orderDetails.product_details}
              renderItem={({item}) => this.RenderItem(item)}
              animationType={AnimationType.SlideFromRight}
              keyExtractor={(item) => JSON.stringify(item.id)}
              animationDuration={1000}
              focused={true}
            />
          </Modal>
        </Portal>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },

  flatlistItem: {
    backgroundColor: '#fff',
    width: '90%',
    height: h('15%'),
    borderRadius: h('1%'),
    flexDirection: 'row',
    elevation: 5,
  },
  Requst: {
    color: 'white',
    fontSize: h('2%'),
    fontFamily: 'HelveticaNowDisplay-Medium',
  },
  btntxt: {
    color: colors.accentBlue,
    fontSize: h('1.8%'),
    marginLeft: h('1%'),
    fontFamily: 'HelveticaNowDisplay-ExtraBold',
  },
  no: {
    color: 'white',
    fontSize: h('4%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  newRequest: {
    backgroundColor: '#fff2',
    width: '80%',
    height: h('15%'),
    flexDirection: 'row',
    borderRadius: h('1%'),
    marginTop: h('4%'),
  },
  left: {
    // backgroundColor: 'green',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  right: {
    // backgroundColor: 'yellow',
    width: '50%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: 'white',
    width: '95%',
    height: h('5%'),
    borderRadius: h('10%'),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: h('2%'),
  },
  flatlistContainer: {
    alignItems: 'center',
    margin: h('2%'),
  },
  ImageBackground: {
    backgroundColor: colors.accentBlue,
    width: '100%',
    height: h('30%'),
    alignItems: 'center',
  },
  cardImage: {
    flex: 1,
    width: '100%',
    borderRadius: 8,
  },
  imgtxt: {
    color: 'white',
    fontSize: h('3%'),
    marginTop: h('3%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  flatlistContainerView: {
    backgroundColor: colors.white,
    height: '100%',
  },
  leftContainer: {
    // backgroundColor: '#000',
    width: '40%',
    height: '100%',
    justifyContent: 'center',
    padding: h('2%'),
  },
  RightContainer: {
    // backgroundColor: 'red',
    width: '60%',
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
  },
  imgcontainer: {
    backgroundColor: '#ea5455',
    borderRadius: h('100%'),
    width: '80%',
    height: '70%',
  },
  LastContainer: {
    // backgroundColor: 'yellow',
    width: '30%',
    alignItems: 'flex-end',
  },
  nametxt: {
    color: 'black',
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  datetxt: {
    color: colors.primary,
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Bold',
  },
  descriptiontxt: {
    color: 'gray',
    fontSize: h('1.8%'),
    fontFamily: 'HelveticaNowDisplay-Regular',
  },
  addresstxt: {
    color: 'silver',
    fontSize: h('2%'),
    // marginTop: h('1%'),
  },
  noConatiner: {
    // backgroundColor: 'red',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: h('10%'),
    marginTop: h('3%'),
    borderColor: '#ea5455',
    borderWidth: 1,
    marginLeft: h('4%'),
  },
  requestContainr: {
    backgroundColor: '#FF215D',
    width: '80%',
    height: '25%',
    justifyContent: 'center',
    paddingLeft: h('2%'),
    // borderRadius: h('10%'),
    marginTop: h('3%'),

    borderTopLeftRadius: h('10%'),
    borderBottomLeftRadius: h('10%'),
  },
  circlebLood: {
    width: '50%',
    height: '42%',
    borderRadius: h('10%'),
    // backgroundColor: 'red',
    marginTop: h('5%'),
    marginRight: h('3%'),
    borderColor: 'silver',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circelTxt: {
    color: '#FF215D',
    fontSize: h('2.5%'),
  },
  requestTxt: {
    color: '#Ffff',
    fontSize: h('2%'),
  },
  frespace: {
    // backgroundColor: 'white',
    width: '100%',
    height: h('5%'),
    // marginTop: -h('50%'),
  },
  labelText: {
    fontFamily: 'HelveticaNowDisplay-Regular',
    fontSize: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
