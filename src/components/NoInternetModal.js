import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Modal,
  ActivityIndicator,
  Image,
} from 'react-native';
import colors from '../assets/colors/colors';
import HeadingText from './HeadingText';
import AnimatedLoader from 'react-native-animated-loader';

const styles = StyleSheet.create({
  modalView: {
    flexDirection: 'row',
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  lottie: {
    height: 200,
    width: 200,
  },
  centeredView: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const NoInternetModal = ({label, modalVisible}) => {
  return (
    <View style={styles.centeredView}>
      <View style={styles.modalView}>
        <AnimatedLoader
          visible={true}
          overlayColor="rgba(255,255,255,0.75)"
          source={require('../../../kathmandubutchery/src/assets/nointernet.json')}
          animationStyle={styles.lottie}
          speed={1}>
          <HeadingText label={'No Internet Connection'} />
        </AnimatedLoader>
      </View>
    </View>
  );
};

export default NoInternetModal;
