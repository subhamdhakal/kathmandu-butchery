import React from 'react';
import colors from '../assets/colors/colors';
import {Text} from 'react-native';

const RegularText = ({label}) => {
  return (
    <Text
      style={{
        fontSize: 14,
        color: colors.darkGreen,
        fontFamily: 'HelveticaNowDisplay-Bold',
      }}>
      {label}
    </Text>
  );
};

export default RegularText;
