/* eslint-disable handle-callback-err */
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Image} from 'react-native';
import {
  widthPercentageToDP as w,
  heightPercentageToDP as h,
} from 'react-native-responsive-screen';
import {Icon} from 'react-native-elements';
import {Surface} from 'react-native-paper';
import {instanceAxios, Url} from '../screens/Api';
import AsyncStorage from '@react-native-community/async-storage';

export class ProfilePic extends Component {
  state = {img: ''};

  capture = () => {
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
  };

  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.capture();
        }}
        {...this.props}
        style={styles.imgbox}>
        <Surface style={styles.surface}>
          {/* <Image source={{uri: this.state.Photo}} /> */}
        </Surface>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  btxt: {
    color: 'white',
    fontSize: h('3%'),
    fontWeight: 'bold',
    marginTop: h('1%'),
  },
  imgbox: {
    // backgroundColor: 'white',
    width: '35%',
    height: h('20%'),
    borderRadius: h('10%'),
    marginTop: h('2%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  surface: {
    padding: 8,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    // backgroundColor: 'red',
    borderRadius: h('10%'),
  },
});
