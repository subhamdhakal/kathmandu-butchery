import React from 'react';
import colors from '../assets/colors/colors';
import {Text} from 'react-native';

const SubHeadingText = ({label}) => {
  return (
    <Text
      style={{
        color: colors.textDarkGray,
        fontFamily: 'HelveticaNowDisplay-Bold',
        fontSize: 12,
      }}>
      {label}
    </Text>
  );
};

export default SubHeadingText;
