import React from "react";
import { Text } from "react-native";

const LabelText = ({ label }) => {
  return (
    <Text
      style={{
        fontFamily: "HelveticaNowDisplay-Bold",
        fontSize: 12,
      }}
    >
      {label}
    </Text>
  );
};

export default LabelText;
